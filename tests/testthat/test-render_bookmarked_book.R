test_that("render_bookmarked_book() works", {

  ### Potentially reinstall tinytex
  # tinytex::reinstall_tinytex()
  ### Potentially install latex packages
  # tinytex::tlmgr_install(
  #   c('koma-script', 'bookmark', 'caption',
  #     'tcolorbox', 'pgf', 'environ', 'pdfcol')
  #  );

  # devtools::load_all(); devtools::document();

  systemPath <-
    system.file("exampleBook1",
                package = "bookmark");

  outputPath <- tempfile();
  dir.create(outputPath);

  res <- bookmark::bm_prepare(
    systemPath,
    silent=FALSE,
    idsToSelectRegex = "6",
    functionList =
      list(
        courseDepFunction = bm_fn_collapse_to_ul,
        md4e_chapter_colofon = function(...) { return("md4e_chapter_colofon results"); },
        md4e_chapter_topics = function(...) { return("md4e_chapter_topics results"); },
        md4e_chapter_courses = function(...) { return("md4e_chapter_courses results"); },
        md4e_chapter_dependencies = function(...) { return("md4e_chapter_dependencies results"); },
        md4e_showLearningGoals = bm_fn_collapse_to_ul
      ),
    functionArgs =
      list(
        courseDepFunction = list(idRegex = "metadata",
                                 keyRegex = "course_id",
                                 entireBook = FALSE),
        md4e_showLearningGoals = list(idRegex = "metadata",
                                      keyRegex = "dependency",
                                      entireBook = FALSE)
      )
  );

  res <- bookmark::bm_render(
    systemPath,
    output = outputPath,
    silent=FALSE,
    idsToSelectRegex = "6",
    functionList =
      list(
        courseDepFunction = bm_fn_collapse_to_ul,
        # md4e_chapter_colofon = bm_fn_collapse_to_ul, # function(...) { return("md4e_chapter_colofon results"); },
        # md4e_chapter_topics = bm_fn_collapse_to_ul, # function(...) { return("md4e_chapter_topics results"); },
        # md4e_chapter_courses = bm_fn_collapse_to_ul, # function(...) { return("md4e_chapter_courses results"); },
        chapter_dependencies = function(...) { return("chapter_dependencies results"); },
        md4e_showLearningGoals = bm_fn_collapse_to_ul
      ),
    functionArgs =
      list(
        courseDepFunction = list(idRegex = "metadata",
                                 keyRegex = "course_id",
                                 entireBook = FALSE),
        md4e_showLearningGoals = list(idRegex = "metadata",
                                      keyRegex = "dependency",
                                      entireBook = FALSE)
      )
  );

  #res$testfile_3$data[names(res$testfile_3$data) == 'topic']


})
