test_that("test parse_bookmarks()", {

  # devtools::load_all(); devtools::document();

  systemPath <-
    system.file("exampleBook1",
                package = "bookmark");

  ### Function one

  courseDepFunction <- function(x,
                                id) {
    return(
      paste0(
        "<div class='courses'>This chapter is used in the following courses:",
        "<ul>",
        paste0("<li>", x$shorthand_values$sortedRes$metadata$course_id, "</li>", collapse=""),
        "</ul>"
      )
    );
  }

  ### Function two

  chaptersByAuthor <- function(x,
                               id,
                               filename = NULL) {

    chapterList <-
      lapply(
        x,
        function(currentBookmark) {
          return(
            list(
              id = currentBookmark$metadata$id,
              title = currentBookmark$data$title,
              authors = currentBookmark$data$authors$name
            )
          );
        }
      );

    res <- unlist(
      lapply(
        chapterList,
        function(currentChapter) {
          if (is.null(currentChapter$title)) {
            titleBit <- currentChapter$id;
          } else {
            titleBit <- currentChapter$title;
          }
          return(paste0("<li>", vecTxt(currentChapter$authors),
                        ". <em>", titleBit, "</em></li>"));
        }
      )
    );

    if (!is.null(filename)) {

      df <- rbind_df_list(
        lapply(
          chapterList,
          function(currentChapter) {

            return(
              data.frame(
                id = currentChapter$id,
                title = ifelse(is.null(currentChapter$title),
                               "",
                               currentChapter$title),
                author = ifelse(is.null(currentChapter$authors),
                                "",
                                vecTxt(currentChapter$authors))
              )
            );
          }
        )
      );

      write.csv(
        df,
        filename,
        row.names = FALSE
      );

    }

    return(paste0(res, collapse="\n"));

  }

  md4e_showLearningGoals <- function(x,
                                     id,
                                     bloomType) {
    return('learning goals I guess');
  }

  md4e_chapter_colofon <- function(x,
                                   id) {
    return('chapter colofon');
  }

  md4e_chapter_topics <- function(x,
                                   id) {
    return('chapter topics');
  }

  md4e_chapter_courses <- function(x,
                                  id) {
    return('chapter courses');
  }

  md4e_chapter_dependencies <- function(x,
                                   id) {
    return('chapter dependencies');
  }

  filename <- tempfile(fileext = ".csv");

  bookmarks <- parse_bookmarks(
    systemPath,
    functionArgs = list(chaptersByAuthor = list(filename = filename))
  );

})
