#' Build a tree from bookmark files
#'
#' This function can be used in functions that operate on imported files with
#' bookmarks. It takes two arguments: `x`, the object with parsed files, and
#' a list called `dependencies` with two elements: the `id` and `key` that hold
#' the dependencies of a given file. This function requires the `data.tree`
#' package to be installed.
#'
#' @param x The object with parsed files as resulting from a call to
#' [select_chapters()] or [parse_bookmarks()].
#' @param dependencies A list with two elements names `id` and `key`, specifying
#' where to find the dependencies of each file.
#'
#' @return A `data.tree` object.
#' @export
#'
#' @examples
build_trees_from_bookmarks <- function(x,
                                       dependencies = list(id = "metadata",
                                                           key = "dependency")) {

  # if (!requireNamespace("data.tree", quietly = TRUE)) {
  #   stop("To use this function, you need the `data.tree` package. You can ",
  #        "install it with:\n\n  install.packages('data.tree');\n");
  # }

  if (!inherits(x, "bookmarks_from_dir")) {
    stop("As `x`, you must pass the product of parsing multiple bookmark ",
         "files. It must have class `bookmarks_from_dir`, whereas the ",
         "object you passed as `x` has class(es) ", vecTxtQ(class(x)),
         ".");
  }

  dfNetwork <-
    rbind_df_list(
      lapply(
        x,
        function(currentFile) {
          if (dependencies$id %in% names(currentFile$data)) {
            if (dependencies$key %in% names(currentFile$data[[dependencies$id]])) {
              return(
                data.frame(
                  rep(currentFile$metadata$id,
                      length(currentFile$data[[dependencies$id]][[dependencies$key]])),
                  currentFile$data[[dependencies$id]][[dependencies$key]]
                )
              );
            } else {
              return(data.frame());
            }
          } else {
            return(data.frame());
          }
        }
      )
    );
  names(dfNetwork) <- c("file", "dependency");

  dependenciesPerFile <-
    lapply(
      names(x),
      dfNetwork_for_file,
      dfNetwork
    );
  names(dependenciesPerFile) <- names(x);

  treePerFile <-
    lapply(
      dependenciesPerFile,
      function(depDf) {
        if (nrow(depDf) > 0) {
          return(data.tree::FromDataFrameNetwork(depDf));
        } else {
          return(NULL);
        }
      }
    );

  return(
    treePerFile
  );

}

dfNetwork_for_file <- function(file,
                               dfNetwork) {
  res <-
    dfNetwork[dfNetwork$file == file, , drop=FALSE];
  if (nrow(res) == 0) {
    return(data.frame());
  } else {
    return(
      rbind_dfs(
        res,
        rbind_df_list(
          lapply(
            res[, 'dependency'],
            dfNetwork_for_file,
            dfNetwork
          )
        )
      )
    );
  }
}
