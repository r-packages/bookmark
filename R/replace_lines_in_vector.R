#' Replace lines in a character vector
#'
#' @param x The original character vector
#' @param replacement A character vector with the new lines
#' @param lines The numbers of the lines where to insert the replacement
#'
#' @return The vector with the specified lines replaced with the new content
#' @export
#'
#' @examples bookmark::replace_lines_in_vector(
#'   x = c(
#'     "example line 1",
#'     "example line 2",
#'     "example line 3",
#'     "example line 4",
#'     "example line 5"
#'   ),
#'   replacement = c(
#'     "replacement line 1, inserted at position 3",
#'     "replacement line 2",
#'     "replacement line 3",
#'     "replacement line 4, inserted before position 5"
#'   ),
#'   lines = 3:4
#' );
#'
#' bookmark::replace_lines_in_vector(
#'   LETTERS[1:10],
#'   letters[8:26],
#'   6:9
#' );
replace_lines_in_vector <- function(x,
                                    replacement,
                                    lines) {

  if (!is.character(x)) {
    stop("What you pass as `x` has to be a character vector. Instead, ",
         "it has class(es) ", vecTxtQ(class(x)), ".");
  }

  if (!is.character(replacement)) {
    stop("What you pass as `replacement` has to be a character vector. ",
         "Instead, it has class(es) ", vecTxtQ(class(replacement)), ".");
  }

  if (length(lines) == 0) {
    stop("The `lines` argument has to have at least one value, but ",
         "has none.");
  } else if (length(lines) == 1) {
    lines <- rep(lines, 2);
  } else if (length(lines) > 2) {
    lines <- range(lines, na.rm=TRUE);
  }

  if ((min(lines) < 1) || (max(lines) > length(x))) {
    stop("The lowest value of the `lines` argument ('", min(lines),
         "') has to be at least 1, and the highest value ('", max(lines),
         "') cannot be higher than the length of vector `x` ('",
         length(x), "'). One of these conditions of violated.");
  }

  if (min(lines) == 1) {
    bitBeforeReplacement <- c();
  } else {
    bitBeforeReplacement <-
      x[1:(min(lines)-1)];
  }

  if (max(lines) == length(x)) {
    bitAfterReplacement <- c();
  } else {
    bitAfterReplacement <-
      x[(max(lines)+1):length(x)];
  }

  res <- c(
    bitBeforeReplacement,
    replacement,
    bitAfterReplacement
  );

  return(res);

}
