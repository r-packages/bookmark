#' Bookmark function to create a list of chapter dependencies
#'
#' This function creates a list of the dependencies of this chapter.
#'
#' @param x,id These are two default arguments that all `{bookmark}` functions
#' must have. As `x`, an object is passed with class "`bm_prepared_book`",
#' containing the bookmarked data and each chapter's contents. As `id`, the
#' identifier of the current chapter (i.e. where the `{bookmark}` function is
#' encountered) is passed (a function can choose to ignore this).
#' @param divClass The class to pass to the `<div>` element containing
#' the result.
#' @param prefix,suffix The prefix and suffix to insert before and after the
#' result (but within the containing `<div>`).
#'
#' @return A character vector with the result.
#' @export
#'
#' @examples
bm_fn_chapter_dependencies <- function(x,
                                       id = NULL,
                                       divClass = "bm-chapterDependencies",
                                       prefix = "",
                                       suffix = "") {

  if ((!inherits(x, "bm_prepared_book")) && (!inherits(x, "bookmarks_from_dir"))) {
    stop("As `x`, you must pass an object with class `bm_prepared_book`, ",
         "containing parsed bookmarks from multiple files, ",
         "as produced by a call to ",
         "bookmark::bm_prepare(). Instead, the object you ",
         "passed has class(es) ", vecTxtQ(class(x)), ".");
  }

  dependencyTrees <- attr(x, "dependencyTrees");

  dependencyTree <- dependencyTrees[[id]];

  if ((!is.null(dependencyTree)) && inherits(dependencyTree, "Node")) {

    depTreeLevels <- dependencyTree$Get("level");
    depTreeIds <- names(dependencyTree$Get("level"));
    depTreeLabels <- bm_get_chapter_label(x = x, chapterId = depTreeIds);

    depTreeSpaces <- repStr((depTreeLevels - 1), str = "\U2508");

    depTreeBulletedList <-
      paste0(
        paste0(depTreeSpaces, "\U2514\U2500\U25BA ", depTreeLabels),
        collapse = "  \n"
      );

    res <-
      paste0(
        "::: {", divClass, "}\n",
        prefix,
        depTreeBulletedList,
        suffix,
        "\n\n:::"
      );

  } else {

    res <-
      paste0(
        "::: {", divClass, "}\n",
        prefix,
        "\n(this chapter has no dependencies)\n",
        suffix,
        "\n\n:::"
      );

  }

  return(
    res
  );

}
