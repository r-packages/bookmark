#' Parse an R fragment
#'
#' This is essentially a wrapper for a call to [base::parse()] wrapped in
#' a call to [base::eval()].
#'
#' @param x The fragment.
#'
#' @return The object with the results.
#' @export
#'
#' @examples ### Parse some R
#' bookmark::parse_fragment_R("
#'   result <- sqrt(9);
#'   anotherResult <- result^2;
#' "
#' );
parse_fragment_R <- function(...) {
  eval(parse(text = unlist(as.list(...))));
  return(
    as.list(
      environment()
    )
  );
}
