
<!-- README.md is generated from README.Rmd. Please edit that file -->

# <img src='man/figures/logo.png' align="right" style="height:300px" /> bookmark 📦

## Extract and process embedded data in R Markdown and Quarto files

<!-- badges: start -->

[![CRAN
status](https://www.r-pkg.org/badges/version/bookmark)](https://cran.r-project.org/package=bookmark)

[![Dependency
status](https://tinyverse.netlify.com/badge/bookmark)](https://CRAN.R-project.org/package=bookmark)

[![Pipeline
status](https://gitlab.com/r-packages/bookmark/badges/main/pipeline.svg)](https://gitlab.com/r-packages/bookmark/-/commits/main)

[![Downloads last
month](https://cranlogs.r-pkg.org/badges/last-month/bookmark?color=brightgreen)](https://cran.r-project.org/package=bookmark)

[![Total
downloads](https://cranlogs.r-pkg.org/badges/grand-total/bookmark?color=brightgreen)](https://cran.r-project.org/package=bookmark)

[![Coverage
status](https://codecov.io/gl/r-packages/bookmark/branch/main/graph/badge.svg)](https://app.codecov.io/gl/r-packages/bookmark?branch=main)

<!-- badges: end -->

The pkgdown website for this project is located at
<https://bookmark.opens.science>. If the development version also has a
pkgdown website, that’s located at
<https://r-packages.gitlab.io/bookmark>.

<!-- - - - - - - - - - - - - - - - - - - - - -->
<!-- Start of a custom bit for every package -->
<!-- - - - - - - - - - - - - - - - - - - - - -->

This package is intended to support embedding metadata in Quarto and
bookdown projects, providing functions to read those metadata and use it
to add the resulting content to the Bookdown and Quarto project.

<!-- - - - - - - - - - - - - - - - - - - - - -->
<!--  End of a custom bit for every package  -->
<!-- - - - - - - - - - - - - - - - - - - - - -->

## Installation

You can install the released version of `bookmark` from
[CRAN](https://CRAN.R-project.org) with:

``` r
install.packages('bookmark');
```

You can install the development version of `bookmark` from
[GitLab](https://about.gitlab.com) with:

``` r
remotes::install_gitlab('r-packages/bookmark');
```

(assuming you have `remotes` installed; otherwise, install that first
using the `install.packages` function)

You can install the cutting edge development version (own risk, don’t
try this at home, etc) of `bookmark` from
[GitLab](https://about.gitlab.com) with:

``` r
remotes::install_gitlab('r-packages/bookmark@dev');
```

<!-- - - - - - - - - - - - - - - - - - - - - -->
<!-- Start of a custom bit for every package -->
<!-- - - - - - - - - - - - - - - - - - - - - -->
<!-- - - - - - - - - - - - - - - - - - - - - -->
<!--  End of a custom bit for every package  -->
<!-- - - - - - - - - - - - - - - - - - - - - -->
